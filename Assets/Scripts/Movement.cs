using Unity.VisualScripting;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 10f;
    [SerializeField] private float jumpForce = 1000f;
    [SerializeField] private KeyCode JumpButton = KeyCode.Space;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Collider2D feetCollider;
    private Rigidbody2D myRigidbody2D;
    private SpriteRenderer spriteRendered;
    private Animator playerAnimator;


    private void Awake()
    {
        myRigidbody2D = GetComponent<Rigidbody2D>();
        spriteRendered = GetComponent<SpriteRenderer>();
        playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flip(playerInput);
        SwitchAnimation(playerInput);
        bool isGrounded = feetCollider.IsTouchingLayers(groundLayer);
        if (Input.GetKeyDown(JumpButton) && isGrounded) Jump();
    }

    private void Move(float direction)
    {
        Vector2 velocity = myRigidbody2D.velocity;
        myRigidbody2D.velocity = new Vector2(speed * direction, velocity.y);
    }

    private void Jump()
    {
        Vector2 JumpVector = new Vector2(0f, jumpForce);
        myRigidbody2D.AddForce(JumpVector);
    }

    private void Flip(float direction)
    {
        if (direction > 0) spriteRendered.flipX = false;
        if (direction < 0) spriteRendered.flipX = true;
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }
}
