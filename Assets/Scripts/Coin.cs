using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private int coinCost = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var layer = 1 << collision.gameObject.layer;
        if((layer & playerLayer) == 0) return;

        PickCoin();
    }
    private void PickCoin()
    {
        Game.game.AddCoins(coinCost);
        Destroy(gameObject);



    }
}
